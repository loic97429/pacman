#!/usr/bin/python2.7
# coding: utf-8

try:
    from Tkinter import Button, Label, Frame, Listbox, ALL
    import tkMessageBox
except Exception:
    from tkinter import Button, Label, Frame, Listbox, ALL
    from tkinter import messagebox as tkMessageBox
import Menu as MenuWindow
import pacmanlabyrinthesuite as game

POSSIBLE_MODES = ["M", "X", "P", "B", "C", "I", "0"]
CURRENT_MODE = 0


def drawMapWithGrid(levelToEdit):
    global canvas, level
    level = levelToEdit
    Button(frame1, text="SAVE", command=saveLevel, fg="yellow", bg="black").grid(row=0, column=2)
    game.createMap()
    canvas = game.canvas1
    game.interprete(level)
    canvas.bind("<Button-1>", callbackClickCanvas)
    for i in range(game.NB_BLOCS):
        for j in range(game.NB_LIGNES):
            canvas.create_rectangle(i * game.LARGEUR_BLOC, j * game.LARGEUR_BLOC, (i + 1) * game.LARGEUR_BLOC, (j + 1) * game.LARGEUR_BLOC, outline="white")


def chooseLevelToEdit(callbackfunc, editing=True):
    global listBox, frameChoose
    if game.existe(game.fichegrille):
        with open(game.fichegrille) as f:
            nbLevels = len(f.readlines())
    else:
        with open(game.fichegrille, "w") as f:
            f.write("")
        nbLevels = 0
    frameChoose = Frame(game.fenetre, bg="black")
    frameChoose.pack()
    Label(frameChoose, text="Choose level to " + ("edit." if editing else "play."), fg="yellow", bg="black").pack()
    listBox = Listbox(frameChoose, fg="yellow", bg="black")
    listBox.pack()
    for i in range(nbLevels):
        listBox.insert(i, "level " + str(i + 1))
    if editing:
        listBox.insert(nbLevels, "CREATE NEW")
        message = "EDIT"
    else:
        message = "PLAY !"
    Button(frameChoose, text=message, command=callbackfunc, fg="yellow", bg="black").pack()


def proceedAfterChoose():
    global listBox, frameChoose, levelIndex
    levelIndex = listBox.curselection()[0]
    frameChoose.pack_forget()
    with open(game.fichegrille) as f:
        lines = f.readlines()
        if levelIndex < len(lines):
            level = lines[levelIndex]
        else:
            level = "0" * game.NB_BLOCS * game.NB_LIGNES
    drawMapWithGrid(level)


def saveLevel():
    if level.count("X") != 1 or level.count("P") != 1 or level.count("I") != 1 or level.count("B") != 1 or level.count("C") != 1:
        tkMessageBox.showerror("Error on save", "Impossible de sauvegardez, assurez vous d'avoir le bon nombre de fantôme et 1 seul pacman !")
        return 0
    with open(game.fichegrille, 'r') as f:
        lines = f.readlines()
        if levelIndex < len(lines):
            lines[levelIndex] = level
        else:
            lines.append(level)
    lines = [e.replace("\n", "").replace(" ", "") for e in lines]
    with open(game.fichegrille, 'w') as f:
        f.write("\n".join(lines))
    tkMessageBox.showinfo("Save", "Votre niveau est sauvegardé !")


def callbackClickCanvas(event):
    global canvas, level
    canvas.delete(ALL)
    for i in range(game.NB_BLOCS):
        for j in range(game.NB_LIGNES):
            canvas.create_rectangle(i * game.LARGEUR_BLOC, j * game.LARGEUR_BLOC, (i + 1) * game.LARGEUR_BLOC, (j + 1) * game.LARGEUR_BLOC, outline="white")
            if (i * game.LARGEUR_BLOC <= event.x  <= (i + 1) * game.LARGEUR_BLOC) and (j * game.LARGEUR_BLOC <= event.y <= (j + 1) * game.LARGEUR_BLOC):
                if POSSIBLE_MODES[CURRENT_MODE] == "C" and level.count("C") == 1:
                    tkMessageBox.showerror("Error", "Vous aves déjà poser Clyde, veuillez le supprimez (mode 0) avant de le replacer !")
                    continue
                if POSSIBLE_MODES[CURRENT_MODE] == "B" and level.count("B") == 1:
                    tkMessageBox.showerror("Error", "Vous aves déjà poser Blinky, veuillez le supprimez (mode 0) avant de le replacer !")
                    continue
                if POSSIBLE_MODES[CURRENT_MODE] == "I" and level.count("I") == 1:
                    tkMessageBox.showerror("Error", "Vous aves déjà poser Inky, veuillez le supprimez (mode 0) avant de le replacer !")
                    continue
                if POSSIBLE_MODES[CURRENT_MODE] == "X" and level.count("X") == 1:
                    tkMessageBox.showerror("Error", "Vous aves déjà poser Pacman, veuillez le supprimez (mode 0) avant de le replacer !")
                    continue
                if POSSIBLE_MODES[CURRENT_MODE] == "P" and level.count("P") == 1:
                    tkMessageBox.showerror("Error", "Vous aves déjà poser Pinky, veuillez le supprimez (mode 0) avant de le replacer !")
                    continue
                level = level[:(j * game.NB_BLOCS + i)] + POSSIBLE_MODES[CURRENT_MODE] + level[((j * game.NB_BLOCS + i) + 1):]
    game.interprete(level)


def menu():
    game.fenetre.destroy()
    MenuWindow.main()


def callbackModeChange(event):
    global CURRENT_MODE, modeLabel
    keyPressed = event.char.upper()
    if keyPressed in POSSIBLE_MODES:
        CURRENT_MODE = POSSIBLE_MODES.index(keyPressed)
        modeLabel.config(text="MODE:" + keyPressed)


def main():
    global fenetre, frame1, modeLabel
    game.initialization()
    fenetre = game.fenetre
    fenetre.bind("<Key>", callbackModeChange)
    chooseLevelToEdit(proceedAfterChoose)
    # ########################### Barre de statut ##################################
    frame1 = Frame(fenetre, bg="black")
    frame1.pack()
    modeLabel = Label(frame1, text="MODE:M", fg="yellow", bg="black")
    modeLabel.grid(row=0, column=0)
    Button(frame1, text="Menu", command=menu, fg="yellow", bg="black").grid(row=0, column=4)
    # ###############################################################################
    game.fenetre.mainloop()


if __name__ == "__main__":
    main()
