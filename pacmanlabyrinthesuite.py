#!/usr/bin/python2.7
# -*- coding: UTF8 -*-

try:
    from Tkinter import Tk, PhotoImage, Canvas, ALL, Button, Label, Frame
    import tkMessageBox
except Exception:
    from tkinter import Tk, PhotoImage, Canvas, ALL, Button, Label, Frame
    from tkinter import messagebox as tkMessageBox
import random
import Menu as MenuWindow
from Editor import chooseLevelToEdit
import Editor
import myimages


def initialization():
    global fenetre, icone, \
        NB_BLOCS, NB_LIGNES, LARGEUR_BLOC, CENTRE, \
        deplacement, direction, directionblinky, directionclyde, directioninky, directionpinky, bougepacman, accelerationx, accelerationy, fichegrille, \
        NIVEAU, listemurs, listeboules, tab_plateau,\
        blinkyimg, pinkyimg, inkyimg, clydeimg, canvas1, points, temps
    fenetre = Tk()
    fenetre.title("Pacman-DubardLoïc_DinnatLaura")
    fenetre.configure(bg="black")
    # icone = PhotoImage(file="pacman.gif")
    icone = PhotoImage(data=myimages.pacman)
    fenetre.tk.call('wm', 'iconphoto', fenetre._w, icone)
    # ############################variables par défault########################################
    NB_BLOCS = 16
    NB_LIGNES = 24
    LARGEUR_BLOC = 25
    CENTRE = LARGEUR_BLOC / 2
    fichegrille = 'niveaux.lvl'
    deplacement = ["droite", "gauche", "haut", "bas"]
    direction = "droite"
    directioninky = deplacement[random.randint(0, len(deplacement) - 1)]
    directionpinky = deplacement[random.randint(0, len(deplacement) - 1)]
    directionclyde = deplacement[random.randint(0, len(deplacement) - 1)]
    directionblinky = deplacement[random.randint(0, len(deplacement) - 1)]
    bougepacman = 1
    accelerationx = 0
    accelerationy = 0
    NIVEAU = 1
    listemurs = []
    listeboules = [[], []]
    tab_plateau = []
    # chargement des images
    # blinkyimg = PhotoImage(file='blinky.GIF')
    # pinkyimg = PhotoImage(file='pinky.GIF')
    # inkyimg = PhotoImage(file='inky.GIF')
    # clydeimg = PhotoImage(file='clyde.GIF')
    blinkyimg = PhotoImage(data=myimages.blinky)
    pinkyimg = PhotoImage(data=myimages.pinky)
    inkyimg = PhotoImage(data=myimages.inky)
    clydeimg = PhotoImage(data=myimages.clyde)
    points = 0
    temps = 0


def createMap():
    global canvas1, tab_plateau
    # ###############################création de la carte########################################
    canvas1 = Canvas(fenetre, width=NB_BLOCS * LARGEUR_BLOC, height=NB_LIGNES * LARGEUR_BLOC, bg="black")
    canvas1.pack()
    for i in range(NB_BLOCS):
        tab_plateau.append([' '] * NB_LIGNES)


def existe(fname):
    try:  # tente la suite ou passe à except si erreur
        f = open(fname, 'r')  # ouvre le fichier en lecture
        f.close()  # referme le fichier
        return True  # tout est ok
    except Exception:  # code exécuté si ça n'a pas marché
        return False  # retourne : ça n'a pas marché


def afficher(NIVEAU):
    global LARGEUR_BLOC, NB_BLOCS, NB_LIGNES
    # interprete le code de la ligne du NIVEAU lu
    print(NIVEAU)
    canvas1.delete(ALL)
    i, j = 0, 0
    for i in range(NB_BLOCS):
        for j in range(NB_LIGNES):
            tab_plateau[i][j] = " "
    # ouvre le fichier s'il existe
    if existe(fichegrille):
        with open(fichegrille, "r") as f:
            lines = f.readlines()
        if NIVEAU >= len(lines):
            tkMessageBox.showinfo("GAGNE", "Vous avez atteint le dernier NIVEAU disponible.")
        else:
            interprete(lines[NIVEAU])
    else:
        tkMessageBox.showerror("ERREUR", "fichier de NIVEAU introuvable")


def interprete(ligne):
    i, j = 0, 0
    global x, y, pacman, listemurs, blinky, clyde, inky, pinky, LARGEUR_BLOC, NB_BLOCS, NB_LIGNES
    for lettre in ligne:
        if lettre == "C" or lettre == "P" or lettre == "M" or lettre == "I" or lettre == "B" or lettre == "0":
            tab_plateau[i][j] = lettre
        elif lettre == "X":  # position initiale du personnage
            x, y = i * LARGEUR_BLOC, j * LARGEUR_BLOC
            pacman = canvas1.create_arc(
                x, y, x + 25, y + 25, fill="yellow", start=45, extent=270)
            # position du perso initial, inutile de la conserver, elle va changer
            tab_plateau[i][j] = " "
        if i < NB_BLOCS - 1:
            i = i + 1
        elif j < NB_LIGNES - 1:
            j = j + 1
            i = 0
    for i in range(NB_BLOCS):
        for j in range(NB_LIGNES):
            if tab_plateau[i][j] == "M":
                listemurs.append(canvas1.create_rectangle(i * LARGEUR_BLOC, j * LARGEUR_BLOC,
                                                          i * LARGEUR_BLOC + 20, j * LARGEUR_BLOC + 20, fill="blue"))
            elif tab_plateau[i][j] == "0":
                listeboules[0].append(canvas1.create_rectangle(i * LARGEUR_BLOC + 10,
                                                               j * LARGEUR_BLOC + 7, i * LARGEUR_BLOC + 15, j * LARGEUR_BLOC + 12, fill="white"))
    for i in range(NB_BLOCS):
        for j in range(NB_LIGNES):
            if tab_plateau[i][j] == "B":
                blinky = canvas1.create_image(i * LARGEUR_BLOC, j * LARGEUR_BLOC,
                                              image=blinkyimg, anchor='nw')
            elif tab_plateau[i][j] == "P":
                pinky = canvas1.create_image(i * LARGEUR_BLOC, j * LARGEUR_BLOC,
                                             image=pinkyimg, anchor='nw')
            elif tab_plateau[i][j] == "I":
                inky = canvas1.create_image(i * LARGEUR_BLOC, j * LARGEUR_BLOC,
                                            image=inkyimg, anchor='nw')
            elif tab_plateau[i][j] == "C":
                clyde = canvas1.create_image(i * LARGEUR_BLOC, j * LARGEUR_BLOC,
                                             image=clydeimg, anchor='nw')


# ##############gestion des niveaux des scores et du resultat (game over ou gagné)#########

def verifgagne():
    if listeboules == [[], []]:
        return True
    else:
        return False


def gameover(x, y):
    xblinky, yblinky = canvas1.coords(blinky)
    xinky, yinky = canvas1.coords(inky)
    xclyde, yclyde = canvas1.coords(clyde)
    xpinky, ypinky = canvas1.coords(pinky)
    if (x == xblinky and y == yblinky) or (x == xpinky and y == ypinky) or (x == xinky and y == yinky)\
            or (x == xclyde and y == yclyde):
        tkMessageBox.showinfo("Game Over", "VOUS AVEZ PERDU, points :" + str(points))
        menu()


def mange(x, y, direction):  # la fonction qui permet de manger les bonbons
    global points
    for i in listeboules[0]:
        a, b, c, d = canvas1.coords(i)
        if (direction == "droite" and ((x >= a - 15 and x <= a) and (y >= b - 15 and y <= b)))\
                or (direction == "gauche" and ((x >= a - 15 and x <= a - 10) and (y >= b - 15 and y <= b - 5)))\
                or (direction == "haut" and ((x >= a - 15 and x <= a - 5) and (y >= b - 15 and y <= b - 5)))\
                or (direction == "bas" and ((x >= a - 15 and x <= a - 5) and (y >= b - 15 and y <= b))):
            canvas1.delete(i)
            listeboules[0].remove(i)
            points += 1
            score.config(text="SCORE :" + str(points))
# ###############################création du personnage pacman#######################


def anime_droite_ouvert():
    canvas1.itemconfig(pacman, start=45, extent=270, fill="yellow")
    fenetre.after(100, anime_fermer)


def anime_droite_ferme():
    canvas1.itemconfig(pacman, start=0, extent=359, fill="yellow")
    fenetre.after(100, anime_ouvrir)


def anime_gauche_ouvert():
    canvas1.itemconfig(pacman, start=225, extent=270, fill="yellow")
    fenetre.after(100, anime_fermer)


def anime_gauche_ferme():
    canvas1.itemconfig(pacman, start=180, extent=359, fill="yellow")
    fenetre.after(100, anime_ouvrir)


def anime_haut_ferme():
    canvas1.itemconfig(pacman, start=90, extent=359, fill="yellow")
    fenetre.after(100, anime_ouvrir)


def anime_haut_ouvert():
    canvas1.itemconfig(pacman, start=135, extent=270, fill="yellow")
    fenetre.after(100, anime_fermer)


def anime_bas_ferme():
    canvas1.itemconfig(pacman, start=270, extent=359, fill="yellow")
    fenetre.after(100, anime_ouvrir)


def anime_bas_ouvert():
    canvas1.itemconfig(pacman, start=315, extent=270, fill="yellow")
    fenetre.after(100, anime_fermer)


def anime_fermer():
    global direction, accelerationx, accelerationy, x, y
    gameover(x, y)
    # appel de la fonction avance pour pacman
    avance(accelerationx, accelerationy)
    if direction == "droite":
        anime_droite_ferme()
    elif direction == "gauche":
        anime_gauche_ferme()
    elif direction == "haut":
        anime_haut_ferme()
    elif direction == "bas":
        anime_bas_ferme()


def anime_ouvrir():
    global direction, accelerationx, accelerationy, x, y
    gameover(x, y)
    if direction == "droite":
        anime_droite_ouvert()
    elif direction == "gauche":
        anime_gauche_ouvert()
    elif direction == "haut":
        anime_haut_ouvert()
    elif direction == "bas":
        anime_bas_ouvert()


# ############################## détection des commandes du joueur ###################


def callback(event):
    global direction, accelerationx, accelerationy, x, y
    if event.keysym == "Up":
        direction = "haut"
        accelerationx = 0
        accelerationy = -25

    elif event.keysym == "Down":
        direction = "bas"
        accelerationx = 0
        accelerationy = 25

    elif event.keysym == "Left":
        direction = "gauche"
        accelerationx = -25
        accelerationy = 0

    elif event.keysym == "Right":
        direction = "droite"
        accelerationx = 25
        accelerationy = 0


def avance(gd, hb):
    global direction, x, y, bougepacman, temps
    global NIVEAU
    if x < 0:			#
        x = 400		#
    elif x > 375:		#
        x = -25  # Zone de delimitation pour la téléportation du Pac-man
    elif y < 0:		#
        y = 600		#
    elif y > 575:		#
        y = -25		#
    if collision(x, y, direction) == 1:
        x, y = x + gd, y + hb
        canvas1.coords(pacman, x, y, x + 25, y + 25)
        mange(x, y, direction)
    if verifgagne():
        tkMessageBox.showinfo("Gagné", 'NIVEAU {} réussi en {}s'.format(NIVEAU, temps))
        NIVEAU = int(NIVEAU) + 1
        tkMessageBox.showinfo("NIVEAU suivant :", "passage au NIVEAU:" + str(NIVEAU))
        niv.config(text="niveau:" + str(NIVEAU))
        temps = 0
        afficher(NIVEAU)

# ############################déplacements automatiques de pacman##########################


def collision(x, y, direction):
    global bougepacman, listemurs
    if direction == "droite":
        bougepacman = 1
        for i in listemurs:
            try:
                a, b, c, d = canvas1.coords(i)
                if ((a - 31 <= x <= a) and (b - 20 <= y <= b + 19)):
                    bougepacman = 0
            except Exception:
                pass
    elif direction == "gauche":
        bougepacman = 1
        for i in listemurs:
            try:
                a, b, c, d = canvas1.coords(i)
                if ((a <= x <= a + 25) and (b - 20 <= y <= b + 19)):
                    bougepacman = 0
            except Exception:
                pass
    elif direction == "haut":
        bougepacman = 1
        for i in listemurs:
            try:
                a, b, c, d = canvas1.coords(i)
                if ((a - 20 <= x <= a + 19) and (b <= y <= b + 25)):
                    bougepacman = 0
            except Exception:
                pass
    elif direction == "bas":
        bougepacman = 1
        for i in listemurs:
            try:
                a, b, c, d = canvas1.coords(i)
                if ((a - 20 <= x <= a + 19) and (b - 31 <= y <= b)):
                    bougepacman = 0
            except Exception:
                pass
    else:
        bougepacman = 1
    return bougepacman

# #############################création des fantômes####################################


def deplacementfantomes(fantome, direction):
    x, y = canvas1.coords(fantome)
    deplacement = ["droite", "gauche", "haut", "bas"]
    for i in listemurs:
        try:
            a, b, c, d = canvas1.coords(i)
            if ((x >= a - 31 and x <= a) and (y >= b - 20 and y <= b + 19)):
                deplacement.remove("droite")
            if ((x >= a and x <= a + 25) and (y >= b - 20 and y <= b + 19)):
                deplacement.remove("gauche")
            if ((x >= a - 20 and x <= a + 19) and (y >= b and y <= b + 25)):
                deplacement.remove("haut")
            if ((x >= a - 20 and x <= a + 19) and (y >= b - 31 and y <= b)):
                deplacement.remove("bas")
            if deplacement != ["droite", "gauche", "haut", "bas"] or random.randint(0, 1) == 0:
                direction = deplacement[random.randint(0, len(deplacement) - 1)]
            return direction
        except Exception:
            pass


def avancefantome(fantome, direction):
    x, y = canvas1.coords(fantome)
    if direction == "droite":
        accelerationx = 25
        accelerationy = 0
    elif direction == "gauche":
        accelerationx = -25
        accelerationy = 0
    elif direction == "haut":
        accelerationx = 0
        accelerationy = -25
    elif direction == "bas":
        accelerationx = 0
        accelerationy = 25
    if x < 10:                       #
        x = 375                      #
        canvas1.coords(fantome, x, y)
    elif x > 375:                    #
        x = 0                        #
        canvas1.coords(fantome, x, y)
    elif y < 5:  # zone de délimitation pour
        y = 575  # la téléportation des fantômes
        canvas1.coords(fantome, x, y)
    elif y > 575:                    #
        y = 0                        #
        canvas1.coords(fantome, x, y)
    if collision(x, y, direction) == 1:
        x, y = x + accelerationx, y + accelerationy
        canvas1.coords(fantome, x, y)
    else:
        avancefantome(fantome, deplacementfantomes(fantome, direction))


def fantomes():
    global directionblinky, directionpinky, directioninky, directionclyde
    directionblinky = deplacementfantomes(blinky, directionblinky)
    avancefantome(blinky, directionblinky)
    directionpinky = deplacementfantomes(pinky, directionpinky)
    avancefantome(pinky, directionpinky)
    directioninky = deplacementfantomes(inky, directioninky)
    avancefantome(inky, directioninky)
    directionclyde = deplacementfantomes(clyde, directionclyde)
    avancefantome(clyde, directionclyde)

    fenetre.after(200, fantomes)


def menu():  # retour au menu
    fenetre.destroy()
    MenuWindow.main()


def countTime():
    global temps, time
    temps += 1
    time.config(text="TIME:{}".format(temps))
    fenetre.after(1000, countTime)


def launchLoops():
    # ############################ lancement des boucles du programme ###########
    fenetre.after(1, fantomes)
    fenetre.after(2, anime_fermer)
    fenetre.after(1, countTime)


def proceedAfterChoose():
    global fichegrille
    # global Editor.listBox, Editor.frameChoose, Editor.levelIndex
    levelIndex = Editor.listBox.curselection()[0]
    Editor.frameChoose.pack_forget()
    createMap()
    afficher(levelIndex)
    launchLoops()
    fenetre.bind("<Key>", callback)


def main():
    global niv, score, time, fenetre, NIVEAU
    initialization()
    chooseLevelToEdit(proceedAfterChoose, False)

    # ########################### Barre de statut ##################################
    frame1 = Frame(fenetre, bg="black")
    frame1.pack()

    Button(frame1, text="Menu", command=menu, fg="yellow", bg="black").grid(row=0, column=4)
    score = Label(frame1, text="SCORE:0", fg="yellow", bg="black")  # affichage des points
    score.grid(row=0, column=2, padx=100)
    time = Label(frame1, text="TIME:0", fg="yellow", bg="black")  # affichage du temps
    time.grid(row=0, column=3, padx=100)
    niv = Label(frame1, text="niveau:" + str(NIVEAU), fg="yellow", bg="black")  # affichage du niveau
    niv.grid(row=0, column=0)

    # ##############################################################################

    fenetre.mainloop()


if __name__ == "__main__":
    main()
