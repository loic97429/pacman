﻿# Pacman Game By Laura Dinnat & Dubard Loïc
## Pour jouer : 
"utilisez les flèches pour déplacer le pacman dans le labyrinthe. 
Le but du jeu est de manger tous les petits points blancs sans se faire manger par les fantômes et en le moins de temps possible"


## Editeur de niveau !
Juste du click sur la grille : 
on peut changer de mode en appuyant sur 
- M pour placer un mur
- X pour placer pacman
- C pour placer clyde
- B pour placer Bliny
- P pour placer Pinky
- I pour placer Inky
- 0 pour supprimer un mur ou un personnage 

## Compilation en un seul fichier executable !
```bash
py -m PyInstaller -F -w -i pacman.ico -n pacman.exe Menu.py
```

