#!/usr/bin/python2.7
# coding: utf-8

try:
    from Tkinter import Tk, PhotoImage, Label, Button, Frame, X
    import tkMessageBox
except Exception:
    from tkinter import Tk, PhotoImage, Label, Button, Frame, X
    from tkinter import messagebox as tkMessageBox
import pacmanlabyrinthesuite as game
import Editor
import myimages


def initialization():
    global mafenetre, icone, photo
    mafenetre = Tk()
    mafenetre.resizable(width=False, height=False)
    mafenetre.title('menu')
    mafenetre['bg'] = 'black'
    icone = PhotoImage(data=myimages.pacman)
    mafenetre.tk.call('wm', 'iconphoto', mafenetre._w, icone)

    photo = PhotoImage(data=myimages.pacman_logo)
    Label(mafenetre, image=photo).grid(row=1, column=1, rowspan=10, columnspan=10)


def jouer():
    mafenetre.destroy()
    game.main()


def edit():
    mafenetre.destroy()
    Editor.main()


def regles():
    Frame1 = Frame(mafenetre, borderwidth=2, bg='blue')
    Frame1.grid(row=30, column=4, padx=10, pady=10, columnspan=20)
    Label(Frame1, text="""
      Pour jouer :
      utilisez les flèches pour déplacer le pacman dans le labyrinthe.
      Le but du jeu est de manger tous les petits points blancs sans se faire manger par les fantômes
          """, fg="blue", bg="white", wraplength=225).pack(fill=X)


def info():
    tkMessageBox.showinfo("INFO", "réalisé par Dubard Loïc et Dinnat Laura dans le cadre du projet pour le bac d'ISN")


def main():
    initialization()

    Button(mafenetre, text="JOUER", fg='blue', command=jouer).grid(
        row=13, column=4, padx=10, pady=10)

    Button(mafenetre, text="EDITOR", fg='blue', command=edit).grid(
        row=13, column=6, padx=10, pady=10)

    Button(mafenetre, text="REGLES", fg='blue', command=regles).grid(
        row=17, column=4, padx=20, pady=20)

    Button(mafenetre, text="INFO", fg="blue", command=info).grid(row=17, column=6)

    Label(mafenetre, bg="black").grid(row=19, column=4)

    mafenetre.mainloop()


if __name__ == "__main__":
    main()
